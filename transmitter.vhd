library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity transmitter is
	port (
		clk        : in  std_logic;                    -- 25Mhz
		data_in    : in  std_logic_vector(7 downto 0); -- ascii from keyboard
		serial_out : out std_logic                     -- rs232 tx
	);
end transmitter;

architecture transmitter of transmitter is
	signal do_transmit  : std_logic;
	signal transmitting : std_logic;
	signal done         : std_logic;
	component serial_tx is
		port (
		 i_Clk       : in  std_logic;
		 i_TX_DV     : in  std_logic;
		 i_TX_Byte   : in  std_logic_vector(7 downto 0); -- data_in
		 o_TX_Active : out std_logic;
		 o_TX_Serial : out std_logic;
		 o_TX_Done   : out std_logic
		);
	end component;
begin
	tx: serial_tx port map(
		i_Clk       => clk,         -- 25Mhz
		i_TX_DV     => do_transmit, -- signal that serial_tx should start transmition
		i_TX_Byte   => data_in,
		o_TX_Active => transmitting,
		o_TX_Serial => serial_out,
		o_TX_Done   => done
	);
	
	process (data_in)
	begin
		if (data_in = "00000000")
		then
			do_transmit <= '0';
		else
			do_transmit <= '1';
		end if;
	end process;
end transmitter;