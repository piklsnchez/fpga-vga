-- control generator
library ieee;
use ieee.std_logic_1164.all;

ENTITY control_generator is
	generic(
		ha: integer := 96;  -- hpulse
		hb: integer := 144; -- hpulse + hbp
		hc: integer := 784; -- hpulse + hbp + hactive
		hd: integer := 800; -- hpulse + hbp + hactive + hfp
		va: integer := 2;   -- vpulse
		vb: integer := 35;  -- vpulse + vbp
		vc: integer := 515; -- vpulse + vbp + vactive
		vd: integer := 525  -- vpulse + vbp + vactive + vfp
	);
	port(
		clk50  : in     std_logic; -- 50MHz
		pclk   : buffer std_logic;
		hsync  : buffer std_logic;
		vsync  : out    std_logic;
		hactive: buffer std_logic;
		vactive: buffer std_logic;
		dena   : out    std_logic -- display enable
	);
END control_generator;

architecture control_generator of control_generator is
begin
	--TODO: nsync, nblank for dac
	
	--pixel clock
	process(clk50)
	begin
		if (rising_edge(clk50))
		then
			pclk <= not pclk;
		end if;
	end process;
	--horizontal
	process(pclk)
		variable hcount: integer range 0 to hd;
	begin
		if (rising_edge(pclk))
		then
			hcount := hcount + 1;
			if (hcount = ha)
			then
				hsync <= '1';
			elsif (hcount = hb)
			then
				hactive <= '1';
			elsif (hcount = hc)
			then
				hactive <= '0';
			elsif (hcount = hd)
			then
				hsync <= '0';
				hcount := 0;
			end if;
		end if;
	end process;
	-- vertical
	process(hsync)
		variable vcount: integer range 0 to vd;
	begin
		if (rising_edge(hsync))
		then
			vcount := vcount + 1;
			if (vcount = va)
			then
				vsync <= '1';
			elsif (vcount = vb)
			then
				vactive <= '1';
			elsif (vcount = vc)
			then
				vactive <= '0';
			elsif (vcount = vd)
			then
				vsync <= '0';
				vcount := 0;
			end if;
		end if;
	end process;
	dena <= hactive and vactive;
end architecture;