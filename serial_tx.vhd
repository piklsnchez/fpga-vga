----------------------------------------------------------------------
-- File Downloaded from http://www.nandland.com
----------------------------------------------------------------------
-- This file contains the UART Transmitter.  This transmitter is able
-- to transmit 8 bits of serial data, one start bit, one stop bit,
-- and no parity bit.  When transmit is complete o_TX_Done will be
-- driven high for one clock cycle.
--
-- Set Generic g_CLKS_PER_BIT as follows:
-- g_CLKS_PER_BIT = (Frequency of i_Clk)/(Frequency of UART)
-- Example: 25 MHz Clock, 115200 baud UART
-- (25000000)/(115200) = 217
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity serial_tx is
	generic (
		g_CLKS_PER_BIT : integer := 217               -- Needs to be set correctly
	);
  port (
    i_Clk       : in  std_logic;                    -- 25Mhz
    i_TX_DV     : in  std_logic;                    -- data ready to send?
    i_TX_Byte   : in  std_logic_vector(7 downto 0);
    o_TX_Active : out std_logic;
    o_TX_Serial : out std_logic;
    o_TX_Done   : out std_logic
    );
end serial_tx;
 
architecture serial_tx of serial_tx is
	type state_type is (s_Idle, s_TX_Start_Bit, s_TX_Data_Bits, s_TX_Stop_Bit, s_Cleanup);
	
	signal state       : state_type                            := s_Idle;
	signal r_Clk_Count : integer range 0 to g_CLKS_PER_BIT - 1 := 0;
	signal r_Bit_Index : integer range 0 to 7                  := 0;               -- 8 Bits Total
	signal r_TX_Data   : std_logic_vector(7 downto 0)          := (others => '0');
	signal r_TX_Done   : std_logic                             := '0';   
begin   
  p_UART_TX : process (i_Clk)
  begin
    if(rising_edge(i_Clk))
	 then         
      case state is 
        when s_Idle =>
          o_TX_Active <= '0';                   -- 0 idle, 1 not idle
          o_TX_Serial <= '1';                   -- Drive Line High for Idle
          r_TX_Done   <= '0';                   -- signal that byte has been sent
          r_Clk_Count <= 0;                     -- counter for time period
          r_Bit_Index <= 0;                     -- index into byte we are sending
			 
          if(i_TX_DV = '1')                     -- data ready to send?
			 then
            r_TX_Data <= i_TX_Byte;             -- this is the byte to send
            state     <= s_TX_Start_Bit;        -- move to next state
          else
            state <= s_Idle;                    -- same state
          end if; 
                                                -- Send out Start Bit. Start bit = 0
        when s_TX_Start_Bit =>
          o_TX_Active <= '1';                   -- not idle
          o_TX_Serial <= '0';                   -- start bit
                                                -- Wait g_CLKS_PER_BIT -1 clock cycles for start bit to finish
          if(r_Clk_Count < g_CLKS_PER_BIT - 1)  -- hold for time
			 then
            r_Clk_Count <= r_Clk_Count + 1;     -- counting time
            state       <= s_TX_Start_Bit;      -- same state
          else
            r_Clk_Count <= 0;
            state       <= s_TX_Data_Bits;
          end if;           
                                                -- Wait g_CLKS_PER_BIT -1 clock cycles for data bits to finish          
		when s_TX_Data_Bits =>
			o_TX_Serial <= r_TX_Data(r_Bit_Index); -- put data bit on the line
			 
          if(r_Clk_Count < g_CLKS_PER_BIT - 1)  -- hold for time
			 then
            r_Clk_Count <= r_Clk_Count + 1;     -- counting time
            state       <= s_TX_Data_Bits;      -- same state
          else
            r_Clk_Count <= 0;                   -- start counter over
                                                -- Check if we have sent out all bits
            if(r_Bit_Index < 7)                 -- not yet
				then
              r_Bit_Index <= r_Bit_Index + 1;   -- move to next bit
              state       <= s_TX_Data_Bits;    -- same state
            else
              r_Bit_Index <= 0;                 -- back to first bit
              state       <= s_TX_Stop_Bit;     -- move to next state
            end if;
          end if; 
                                                -- Send out Stop bit.  Stop bit = 1
        when s_TX_Stop_Bit =>
          o_TX_Serial <= '1';                   -- stop bit
                                                -- Wait g_CLKS_PER_BIT -1 clock cycles for Stop bit to finish
          if(r_Clk_Count < g_CLKS_PER_BIT - 1)  -- hold for time
			 then
            r_Clk_Count <= r_Clk_Count + 1;     -- counting time
            state       <= s_TX_Stop_Bit;       -- same state
          else
            r_TX_Done   <= '1';                 -- we wrote the byte
            r_Clk_Count <= 0;                   -- reset the clock
            state       <= s_Cleanup;           -- move to next state
          end if;         
                                                -- Stay here 1 clock
        when s_Cleanup =>
          o_TX_Active <= '0';
          r_TX_Done   <= '1';
          state       <= s_Idle;                -- move to next state
        when others =>
          state <= s_Idle;                      -- default to idle state
      end case;
    end if;
  end process p_UART_TX; 
  o_TX_Done <= r_TX_Done;                       -- output done signal
end serial_tx;