-- main
library ieee;
use ieee.std_logic_1164.all;

ENTITY main is
	port(
		clk50        : in     std_logic;                    -- internal crystal clock (50Mhz)
		rst          : in     std_logic;                    -- reset line
		serial_in    : in     std_logic;                    -- serial data from external rs232 device
		keyclk       : in     std_logic;                    -- psclk from external keyboard
		ps2data      : in     std_logic;                    -- ps2data from external keyboard 
		serial_out   : out    std_logic;                    -- serial data to external rs232 device
		r            : out    std_logic_vector(9 downto 0); -- red to monitor
		g            : out    std_logic_vector(9 downto 0); -- green to monitor
		b            : out    std_logic_vector(9 downto 0); -- blue to monitor
		hsync        : buffer std_logic;                    -- hsync to monitor
		vsync        : buffer std_logic                     -- vsync to monitor
	);
END main;

architecture main of main is
	signal pclk          : std_logic;                    -- divided down pixel clock
	signal hactive       : std_logic;                    -- active horizontal area of monitor
	signal vactive       : std_logic;                    -- active vertical area of monitor
	signal dena          : std_logic;                    -- display enable for monitor
	signal we            : std_logic;                    -- write enable for ram
	signal read_address  : integer range 0 to (80 * 40); -- read address of ram
	signal write_address : integer range 0 to (80 * 40); -- write address of ram
	signal data_in       : std_logic_vector(7 downto 0); -- data in of ram
	signal data_out      : std_logic_vector(7 downto 0); -- data out of ram
	signal ascii_byte    : std_logic_vector(7 downto 0); -- from keyboad to transmitter
	
	component ram is
		port(
		  clock         : IN  STD_LOGIC;
		  data          : IN  STD_LOGIC_VECTOR (7 DOWNTO 0);
		  write_address : IN  INTEGER RANGE 0 to (80 * 40);
		  read_address  : IN  INTEGER RANGE 0 to (80 * 40);
		  we            : IN  STD_LOGIC;
		  q             : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
		);
	end component;
	
	component keyboard is
		port (
			clk       : in  std_logic;                   -- 25Mhz
			ps2clk    : in  std_logic;                   -- 
			ps2data   : in  std_logic;
			ascii_out : out std_logic_vector(7 downto 0) -- convert make/break codes to ascii
		);
	end component;
	
	component receiver is
		port(
			clk       : in  std_logic; -- 25Mhz
			serial_in : in  std_logic;
			we        : out std_logic;
			address   : out integer range 0 to (80 * 40);
			data_out  : out std_logic_vector(7 downto 0)
		);
	end component;
	
	component transmitter is
		port(				
			clk        : in  std_logic;                    -- 25Mhz
			data_in    : in  std_logic_vector(7 downto 0); -- ascii out from keyboard
			serial_out : out std_logic
		);
	end component;
		
	component image_generator is
		port(
			pclk    : in  std_logic;
			hsync   : in  std_logic;
			hactive : in  std_logic;
			vactive : in  std_logic;
			dena    : in  std_logic;
			address : out integer range 0 to (80 * 40);
			data_in : in  std_logic_vector(7 downto 0);
			r       : out std_logic_vector(9 downto 0);
			g       : out std_logic_vector(9 downto 0);
			b       : out std_logic_vector(9 downto 0)
		);
	end component;
	
	component control_generator is
		generic(
			ha: integer := 96;  -- hpulse
			hb: integer := 144; -- hpulse + hbp
			hc: integer := 784; -- hpulse + hbp + hactive
			hd: integer := 800; -- hpulse + hbp + hactive + hfp
			va: integer := 2;   -- vpulse
			vb: integer := 35;  -- vpulse + vbp
			vc: integer := 515; -- vpulse + vbp + vactive
			vd: integer := 525  -- vpulse + vbp + vactive + vfp
		);
		port(
			clk50   : in     std_logic; -- 50MHz
			pclk    : buffer std_logic;
			hsync   : buffer std_logic;
			vsync   : out    std_logic;
			hactive : buffer std_logic;
			vactive : buffer std_logic;
			dena    : out    std_logic
		);
	end component;
begin
	myram: ram port map(	
		clock         => pclk,
		we            => we,
		read_address  => read_address,
		write_address => write_address,
		data          => data_in,
		q             => data_out
	);
	-- keyboard
	ps2keyboard: keyboard port map(
		clk       => pclk,
		ps2clk    => keyclk,
		ps2data   => ps2data,
		ascii_out => ascii_byte
	);
	-- serial rx
	serial_receiver: receiver port map(
		clk       => pclk,
		serial_in => serial_in,
		we        => we,
		address   => write_address,
		data_out  => data_in
	);
	-- serial tx
	serial_transmitter: transmitter port map(		
		clk        => pclk,
		data_in    => ascii_byte, -- from keyboard
		serial_out => serial_out
	);
	-- image generator
	image: image_generator port map(
		pclk         => pclk,    --pclk: in std_logic;
		hsync        => hsync,   --hsync: in std_logic;
		hactive      => hactive, --hactive: in std_logic;
		vactive      => vactive, --vactive: in std_logic;
		dena         => dena,    --dena: in std_logic;
		address      => read_address,
		data_in      => data_out,
		r            => r,       --r: out std_logic_vector(9 downto 0);
		g            => g,       --g: out std_logic_vector(9 downto 0);
		b            => b        --b: out std_logic_vector(9 downto 0)
	);
	control: control_generator port map(
		clk50,  --clk50: in std_logic; -- 50MHz
		pclk,   --pclk: buffer std_logic; -- 25MHz
		hsync,  --hsync: buffer std_logic;
		vsync,  --vsync: out std_logic;
		hactive,--hactive: buffer std_logic;
		vactive,--vactive: buffer std_logic;
		dena    --dena: out std_logic -- display enable
	);
end architecture;