library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.all;

entity receiver is
	port (
		clk       : in  std_logic; -- 25Mhz
		serial_in : in  std_logic;
		we        : out std_logic; -- to memory
		address   : out integer range 0 to (80 * 40);
		data_out  : out std_logic_vector(7 downto 0) -- to memory; convert to ascii frist
	);
end receiver;

architecture receiver of receiver is
	signal done : std_logic;
	signal data : std_logic_vector(7 downto 0);
	signal addr : integer range 0 to (80 * 40);
	component serial_rx is
		port (
			i_Clk       : in  std_logic; -- 25Mhz
			i_RX_Serial : in  std_logic;
			o_RX_DV     : out std_logic;
			o_RX_Byte   : out std_logic_vector(7 downto 0)
		);
	end component;
begin
	rx: serial_rx port map(
		i_Clk       => clk, -- 25Mhz
		i_RX_Serial => serial_in,
		o_RX_DV     => done,
		o_RX_Byte   => data
	);
	process (clk, done, data) --TODO: problem with end of screen transitioning
	begin
		if(rising_edge(clk))
		then
			if(done = '1') -- serial byte has been read
			then
				we <= '1';
				if(addr > (80*40)) -- max screen size
				then
					addr <= 0;
				else
					if (data = "00011011") -- 27; escape sequence
					then
						addr <= addr;
						we <= '0';
						--set flag that the next byte is an escape sequence; like [, etc.
					elsif(data = "00001101") -- \r
					then
						addr <= (addr - (addr rem 80)) - 1;
						we <= '0';
					elsif(data = "00001010") -- \n
					then
						addr <= (addr + 80);
						we <= '0';
					elsif(data = "00001000") -- backspace
					then
						addr <= addr -1;
					else
						addr <= addr +1;
					end if;
				end if;
			else
				we <= '0';
			end if;-- done
		end if;
		end process;
	address <= addr;
	data_out <= data;
end receiver;