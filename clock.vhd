--CLOCK
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY clock is
	generic (fclk: integer := 25_000_000); -- system clock
	port(
		clk      : in  std_logic;
		rst      : in  std_logic;
		we       : out std_logic;
		enable   : out std_logic;
		address  : out integer range 0 to 1023;
		data_out : out std_logic_vector(7 downto 0)
	);
END clock;

architecture clock of clock is
	--ascii conversion
	constant decimalOffset : integer := 48;
begin
	process (clk, rst)
		variable oneSecond: natural range 0 to fclk;
		variable hourT    : natural range 0 to 3;
		variable hourO    : natural range 0 to 10;
		variable minT     : natural range 0 to 6;
		variable minO     : natural range 0 to 10;
		variable secT     : natural range 0 to 6;
		variable secO     : natural range 0 to 10;
		variable position : natural range 0 to 8;
		variable update   : boolean;		
	begin
		if (rst = '1')
		then
			oneSecond := 0;
			hourT     := 0;
			hourO     := 0;
			minT      := 0;
			minO      := 0;
			secT      := 0;
			secO      := 0;
		elsif (rising_edge(clk))
		then
			oneSecond := oneSecond + 1;
			if (oneSecond = fclk)
			then
				oneSecond := 0;
				secO      := secO + 1;
				-- Update once a second
				update := true;
				if (secO = 10)
				then
					secO := 0;
					secT := secT +1;
					if (secT = 6)
					then
						secT := 0;
						minO := minO + 1;
						if (minO = 10)
						then
							minO := 0;
							minT := minT + 1;
							if (minT = 6)
							then
								minT  := 0;
								hourO := hourO + 1;
								if ((hourT /= 2 and hourO = 10) or (hourT = 2 and hourO = 4))
								then
									hourO := 0;
									hourT := hourT + 1;
									if (hourT = 3)
									then
										hourT := 0;
									end if;
								end if;
							end if;
						end if;
					end if;
				end if;
			end if;
			
			if(update)
			then
				--write buffer to memory
				enable <= '1';
				address <= position;
				we <= '1';
				case position is
					when 0 =>
						data_out <= std_logic_vector(to_unsigned(hourT, data_out'length) + decimalOffset);
					when 1 =>
						data_out <= std_logic_vector(to_unsigned(hourO, data_out'length) + decimalOffset);
					when 2 =>
						data_out <= x"3A";--":";
					when 3 =>
						data_out <= std_logic_vector(to_unsigned(minT, data_out'length) + decimalOffset);
					when 4 =>
						data_out <= std_logic_vector(to_unsigned(minO, data_out'length) + decimalOffset);
					when 5 =>
						data_out <= x"3A";--":";
					when 6 =>
						data_out <= std_logic_vector(to_unsigned(secT, data_out'length) + decimalOffset);
					when 7 =>
						data_out <= std_logic_vector(to_unsigned(secO, data_out'length) + decimalOffset);
					when others =>
						data_out <= (others => '0');
				end case;
				
				if(position = 8)
				then
					position := 0;
					update := false;
					we <= '0';
				end if;
				position := position + 1;
			else
				enable <= '0';
			end if;
		end if; -- rising edge
	end process;
end clock;