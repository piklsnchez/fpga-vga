library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
use ieee.std_logic_arith.conv_std_logic_vector;

entity keyboard is -- ps2 in ascii out
	generic(
		deb_cycles  : integer := 100; --4us debounce @ 25mhz
		idle_cycles : integer := 1500 --60us 
	);
	port (
		clk     : in std_logic; -- 25Mhz
		ps2clk  : in std_logic;
		ps2data : in std_logic;
		ascii_out : out std_logic_vector(7 downto 0)
	);
end keyboard;

architecture keyboard of keyboard is
	signal deb_ps2clk  : std_logic;                     -- debounce clock
	signal deb_ps2data : std_logic;                     -- debounce data
	signal data        : std_logic_vector(10 downto 0);
	signal dout        : std_logic_vector(10 downto 0);
	signal idle        : std_logic;                     -- '1' when data line is idle
	signal error       : std_logic;                     -- '1' when start, stop, or parity wrong
	signal break_code  : std_logic;

begin
------------- debounce ps2clk ----------------
	process(clk)
		variable count: integer range 0 to deb_cycles;
	begin
		if(rising_edge(clk))
		then
			if(deb_ps2clk = ps2clk)   -- signal stays steady
			then
				count := 0;
			else
				count := count + 1;
				if(count = deb_cycles) -- for this amount of time
				then
					deb_ps2clk <= ps2clk;
					count := 0;
				end if;
			end if;
		else
		end if;
	end process;                    -- debounce ps2clk
------------- debounce ps2data ---------------------
	process(clk)
		variable count: integer range 0 to deb_cycles;
	begin
		if(rising_edge(clk))
		then
			if(deb_ps2data = ps2data)
			then
				count := 0;
			else
				count := count + 1;
				if(count = deb_cycles)
				then
					deb_ps2data <= ps2data;
					count := 0;
				end if;
			end if;
		end if;
	end process; ---------- debounce ps2data
---------------------  Idle state ----------------------------
	process(clk)
		variable count: integer range 0 to idle_cycles;
	begin
		if(falling_edge(clk))
		then
			if (deb_ps2data = '0')
			then
				idle <= '0';
				count := 0;
			elsif (deb_ps2clk = '1')
			then
				count := count + 1;
				if(count = idle_cycles)
				then
					--TODO: ignore break code
					if(idle = '0')-- and error = '0')
					then
					    if break_code = '1'
					    then
					        break_code <= '0';
					    else
							case dout(8 downto 1) is
								when x"0e" => ascii_out <= std_logic_vector(to_unsigned(character'pos('`'),8));-- ~; with shift
								when x"16" => ascii_out <= std_logic_vector(to_unsigned(character'pos('1'),8));-- !
								when x"1e" => ascii_out <= std_logic_vector(to_unsigned(character'pos('2'),8));-- @
								when x"26" => ascii_out <= std_logic_vector(to_unsigned(character'pos('3'),8));-- #
								when x"25" => ascii_out <= std_logic_vector(to_unsigned(character'pos('4'),8));-- $
								when x"2e" => ascii_out <= std_logic_vector(to_unsigned(character'pos('5'),8));-- % E
								when x"36" => ascii_out <= std_logic_vector(to_unsigned(character'pos('6'),8));-- ^
								when x"3d" => ascii_out <= std_logic_vector(to_unsigned(character'pos('7'),8));-- &
								when x"3e" => ascii_out <= std_logic_vector(to_unsigned(character'pos('8'),8));-- *
								when x"46" => ascii_out <= std_logic_vector(to_unsigned(character'pos('9'),8));-- (
								when x"45" => ascii_out <= std_logic_vector(to_unsigned(character'pos('0'),8));-- )
								when x"4e" => ascii_out <= std_logic_vector(to_unsigned(character'pos('-'),8));-- _
								when x"55" => ascii_out <= std_logic_vector(to_unsigned(character'pos('='),8));-- +
								when x"66" => ascii_out <= x"08";-- Backspace
								when x"0d" => ascii_out <= x"09";-- Tab
								when x"15" => ascii_out <= std_logic_vector(to_unsigned(character'pos('Q'),8));--
								when x"1d" => ascii_out <= std_logic_vector(to_unsigned(character'pos('W'),8));--
								when x"24" => ascii_out <= std_logic_vector(to_unsigned(character'pos('E'),8));--
								when x"2d" => ascii_out <= std_logic_vector(to_unsigned(character'pos('R'),8));--
								when x"2c" => ascii_out <= std_logic_vector(to_unsigned(character'pos('T'),8));--
								when x"35" => ascii_out <= std_logic_vector(to_unsigned(character'pos('Y'),8));--
								when x"3c" => ascii_out <= std_logic_vector(to_unsigned(character'pos('U'),8));--
								when x"43" => ascii_out <= std_logic_vector(to_unsigned(character'pos('I'),8));--
								when x"44" => ascii_out <= std_logic_vector(to_unsigned(character'pos('O'),8));--
								when x"4d" => ascii_out <= std_logic_vector(to_unsigned(character'pos('P'),8));--
								when x"54" => ascii_out <= std_logic_vector(to_unsigned(character'pos('['),8));-- {
								when x"5b" => ascii_out <= std_logic_vector(to_unsigned(character'pos(']'),8));-- }
								when x"5c" => ascii_out <= std_logic_vector(to_unsigned(character'pos('\'),8));-- |
	--							when x"14" => ascii_out <= character'pos('CapsLock')--
								when x"1c" => ascii_out <= std_logic_vector(to_unsigned(character'pos('A'),8));--
								when x"1b" => ascii_out <= std_logic_vector(to_unsigned(character'pos('S'),8));--
								when x"23" => ascii_out <= std_logic_vector(to_unsigned(character'pos('D'),8));--
								when x"2b" => ascii_out <= std_logic_vector(to_unsigned(character'pos('F'),8));--
								when x"34" => ascii_out <= std_logic_vector(to_unsigned(character'pos('G'),8));--
								when x"33" => ascii_out <= std_logic_vector(to_unsigned(character'pos('H'),8));--
								when x"3b" => ascii_out <= std_logic_vector(to_unsigned(character'pos('J'),8));--
								when x"42" => ascii_out <= std_logic_vector(to_unsigned(character'pos('K'),8));--
								when x"4b" => ascii_out <= std_logic_vector(to_unsigned(character'pos('L'),8));--
								when x"4c" => ascii_out <= std_logic_vector(to_unsigned(character'pos(';'),8));-- :
								when x"52" => ascii_out <= std_logic_vector(to_unsigned(character'pos('''),8));-- "
	--							when x"00" => ascii_out <= character'pos('non-US-1')--
								when x"5a" => ascii_out <= x"13";--
	--							when x"12" => ascii_out <= character'pos('LShift')--
								when x"1a" => ascii_out <= std_logic_vector(to_unsigned(character'pos('Z'),8));--
								when x"22" => ascii_out <= std_logic_vector(to_unsigned(character'pos('X'),8));--
								when x"21" => ascii_out <= std_logic_vector(to_unsigned(character'pos('C'),8));--
								when x"2a" => ascii_out <= std_logic_vector(to_unsigned(character'pos('V'),8));--
								when x"32" => ascii_out <= std_logic_vector(to_unsigned(character'pos('B'),8));--
								when x"31" => ascii_out <= std_logic_vector(to_unsigned(character'pos('N'),8));--
								when x"3a" => ascii_out <= std_logic_vector(to_unsigned(character'pos('M'),8));--
								when x"41" => ascii_out <= std_logic_vector(to_unsigned(character'pos(','),8));-- <
								when x"49" => ascii_out <= std_logic_vector(to_unsigned(character'pos('.'),8));-- >
								when x"4a" => ascii_out <= std_logic_vector(to_unsigned(character'pos('/'),8));-- ?
	--							when x"59" => ascii_out <= character'pos(' RShift')--
	--							when x"11" => ascii_out <= character'pos('LCtrl')--
	--							when x"19" => ascii_out <= character'pos('LAlt')--
								when x"29" => ascii_out <= std_logic_vector(to_unsigned(character'pos(' '),8));--
	--							when x"76" => ascii_out <= character'pos('Esc')--
	--							when x"05" => ascii_out <= character'pos('F1')--
	--							when x"06" => ascii_out <= character'pos('F2')--
	--							when x"04" => ascii_out <= character'pos('F3')--
	--							when x"0c" => ascii_out <= character'pos('F4')--
	--							when x"03" => ascii_out <= character'pos('F5')--
	--							when x"0b" => ascii_out <= character'pos('F6')--
	--							when x"83" => ascii_out <= character'pos('F7')--
	--							when x"0a" => ascii_out <= character'pos('F8')--
	--							when x"01" => ascii_out <= character'pos('F9')--
	--							when x"09" => ascii_out <= character'pos('F10')--
	--							when x"78" => ascii_out <= character'pos('F11')--
	--							when x"07" => ascii_out <= character'pos('F12')--
	                            when x"f0" => break_code <= '1'; -- break code inprogress
	                                ascii_out <= "00000000";
								when others => ascii_out <= "00000000";
							end case;
						end if;-- break_code
					else
						ascii_out <= "00000000";
					end if;--idle = '0'
					idle <= '1'; -- when we hit the idle state the data should be ready to display	
				end if;
			else
				count := 0;
			end if;
		end if;
	end process;--------------------- Idle state
-------------------  receive data --------------
	process(deb_ps2clk)
		variable i: integer range 0 to 15;
	begin
		if(falling_edge(deb_ps2clk))
		then
			if(idle = '1')
			then
				i := 0;
			else
				data(i) <= deb_ps2data;
				i := i + 1;
				if(i = 11)
				then
					i := 0;
					dout <= data;
				end if;
			end if;
		end if;
	end process;----------------- receive data
-----------------  error check ----------------
	process(dout)
	begin
		if(dout(0) = '0' and dout(10) = '1' and (dout(1) xor dout(2) xor dout(3) xor dout(4) xor dout(5) xor dout(6) xor dout(7) xor dout(8) xor dout(9)) = '1')
		then
			error <= '0';
		else
			error <= '1';
		end if;
	end process; --------------- error check
end keyboard;