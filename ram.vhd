----ram
--library ieee;
--use ieee.std_logic_1164.all;
--
--ENTITY ram is
--	port(
--		clk       : in  std_logic;
--		we1       : in  std_logic;
--		enable1   : in  std_logic;
--		we2       : in  std_logic;
--		enable2   : in  std_logic;
--		address1  : in  integer range 0 to (80 * 40);
--		address2  : in  integer range 0 to (80 * 40);
--		data_in1  : in  std_logic_vector(7 downto 0);
--		data_in2  : in  std_logic_vector(7 downto 0);
--		data_out1 : out std_logic_vector(7 downto 0);
--		data_out2 : out std_logic_vector(7 downto 0)
--	);
--end ram;
--ARCHITECTURE ram of ram is
--	type memory is array (0 to (80 * 40)) of std_logic_vector(7 downto 0);
--	signal myram : memory;
--	--ATTRIBUTE ramstyle: STRING;
--	--ATTRIBUTE ramstyle of myram: SIGNAL is "M512";
--begin
--	process(clk, enable1, myram, address1)
--	begin
--		if(rising_edge(clk) and enable1 = '1')
--		then
--			if(we1 = '1')
--			then
--				myram(address1) <= data_in1;
--			end if;
--		end if;
--		data_out1 <= myram(address1);
--	end process;
--	
--	process(clk, enable2, myram, address2)
--	begin
--		if(rising_edge(clk) and enable2 = '1')
--		then
--			if(we2 = '1')
--			then
--				myram(address2) <= data_in2;
--			end if;
--		end if;
--		data_out2 <= myram(address2);
--	end process;
--end ram;

LIBRARY ieee;
USE ieee.std_logic_1164.all;
ENTITY ram IS
  PORT (
    clock         : IN STD_LOGIC;
    data          : IN STD_LOGIC_VECTOR (7 DOWNTO 0);
    write_address : IN INTEGER RANGE 0 to (80 * 40);
    read_address  : IN INTEGER RANGE 0 to (80 * 40);
    we            : IN STD_LOGIC;
    q             : OUT STD_LOGIC_VECTOR (7 DOWNTO 0)
  );
END ram;

ARCHITECTURE rtl OF ram IS
  TYPE MEM IS ARRAY(0 TO (80 * 40)) OF STD_LOGIC_VECTOR(7 DOWNTO 0);
  SIGNAL ram_block: MEM;
  SIGNAL read_address_reg: INTEGER RANGE 0 to (80 * 40);
BEGIN
  PROCESS (clock)
  BEGIN
    IF (rising_edge(clock)) THEN
      IF (we = '1') THEN
        ram_block(write_address) <= data;
      END IF;
      read_address_reg <= read_address;
    END IF;
  END PROCESS;
  q <= ram_block(read_address_reg);
END rtl;